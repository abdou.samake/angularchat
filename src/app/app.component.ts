import { Component } from '@angular/core';
import * as firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyDA3qdkO_HKe4fIpv0Pt8G_yQLYUO5f5_w',
  databaseURL: 'https://angularchat-9b3f9.firebaseio.com'
};
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'new-chat';

  constructor() {
    firebase.initializeApp(config);
  }
}
